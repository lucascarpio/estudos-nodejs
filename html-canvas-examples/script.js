/**
 * Created by makros on 14/01/15.
 */

var backgroundColor = "rgb(41,155,243)";
var colorDetail ="rgba(255, 255, 255, 1.0)";

window.onload = function () {

    /* ctx.fillStyle = "#c82124"; //red
     ctx.beginPath();
     ctx.arc(15,15,15,0,Math.PI*2,true);
     ctx.closePath();
     ctx.fill();

     ctx.fillStyle = "#3370d4"; //blue
     ctx.beginPath();
     ctx.arc(45,20,20,0,Math.PI*2,true);
     ctx.closePath();
     ctx.stroke();
     ctx.fill();*/
    draw();
}
var draw = function() {

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = backgroundColor;
    ctx.beginPath();
    ctx.moveTo(10, 10);
    ctx.lineTo(165, 10);
    ctx.quadraticCurveTo(190, 10, 190, 35);
    ctx.lineTo(190, 165);
    ctx.quadraticCurveTo(190, 190, 165, 190);
    ctx.lineTo(35, 190);
    ctx.quadraticCurveTo(10, 190, 10, 165);
    ctx.lineTo(10, 35);
    ctx.quadraticCurveTo(10, 10, 35, 10);
    ctx.fill();

    ctx.fillStyle = colorDetail;
    ctx.beginPath();
    ctx.arc(110, 25, 5, 0, Math.PI * 2, true);
    ctx.fill();


    ctx.beginPath();
    ctx.arc(135, 25, 7, 0, Math.PI * 2, true);
    ctx.fill();


    ctx.beginPath();
    ctx.arc(65, 175, 7, 0, Math.PI * 2, true);
    ctx.fill();


    ctx.beginPath();
    ctx.arc(90, 175, 5, 0, Math.PI * 2, true);
    ctx.fill();

    ctx.beginPath();
    ctx.moveTo(150, 19);
    ctx.lineTo(150, 31);
    ctx.lineTo(170, 31);
    ctx.lineTo(170, 54);
    ctx.lineTo(182, 54);
    ctx.lineTo(182, 31);
    ctx.lineTo(182, 19);
    ctx.closePath();
    ctx.fill();


    ctx.beginPath();
    ctx.moveTo(18, 150);
    ctx.lineTo(30, 150);
    ctx.lineTo(30, 169);
    ctx.lineTo(51, 169);
    ctx.lineTo(51, 181);
    ctx.lineTo(18, 181);
    ctx.closePath();
    ctx.fill();

    ctx.font = '90px sans-serif';
    ctx.textBaseline = 'bottom';
    ctx.fillText('App', 20, 140);

    ctx.fillStyle = "rgba(180, 218, 246, 0.3)";
    ctx.beginPath();
    ctx.moveTo(10, 10);
    ctx.lineTo(165, 10);
    ctx.quadraticCurveTo(190, 10, 190, 35);
    ctx.lineTo(190, 75);
    ctx.quadraticCurveTo(190, 100, 100, 100);
    ctx.lineTo(100, 100);
    ctx.quadraticCurveTo(10, 100, 10, 75);
    ctx.lineTo(10, 35);
    ctx.quadraticCurveTo(10, 10, 35, 10);
    ctx.fill();

    ctx.fillStyle = "rgba(0, 0, 0, 1.0)";
    ctx.beginPath();
    ctx.moveTo(110, 150);
    ctx.lineTo(186, 150);
    ctx.quadraticCurveTo(196, 150, 196, 160);
    ctx.lineTo(196, 186);
    ctx.quadraticCurveTo(196, 196, 186, 196);
    ctx.lineTo(120, 196);
    ctx.quadraticCurveTo(110, 196, 110, 186);
    ctx.lineTo(110, 160);
    ctx.quadraticCurveTo(110, 150, 120, 150);
    ctx.fill();

    ctx.fillStyle = backgroundColor;
    ctx.fillRect(129, 158, 50, 30);

    ctx.fillStyle = colorDetail;
    ctx.beginPath();
    ctx.arc(120, 173.5, 4, 0, Math.PI * 2, true);
    ctx.fill();

    ctx.font = '22px sans-serif';
    ctx.textBaseline = 'bottom';
    ctx.fillText('com', 134, 183.7);
}

var mudar = function(type, value) {

    switch(type) {
        case 0:
            backgroundColor = value;
            break;
        case 1:
            colorDetail = value;
    }
    draw();
}
