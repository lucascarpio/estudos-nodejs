var router = require('express').Router();

var home = require('../controllers/home');

router.get('/', home.index);
router.post('/login', home.login);
router.get('/cadastrar', home.show_register);
router.post('/register', home.register);
router.get('/sair', home.logout);
module.exports = router;