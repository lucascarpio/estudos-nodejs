var router = require('express').Router();

var botbox = require('../controllers/botbox');

router.get('/', botbox.index);

module.exports = router;