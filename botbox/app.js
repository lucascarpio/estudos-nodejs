const KEY = 'botbox.sid', SECRET = 'botbox';
var express = require('express'),
    app = express(),
    path = require('path'),
    cookieParser = require('cookie-parser')(SECRET),
    store = new (require('express-session')).MemoryStore(),
    sessOpts = {secret: SECRET, key: KEY, store: store, saveUninitialized: true, resave: true},
    bodyParser = require('body-parser'),
    session = require('express-session')(sessOpts),
    logger = require('morgan'),
    methodOverride = require('method-override'),
    debug = require('debug')('ntalk-structured'),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    mongoose = require('mongoose');

global.db = mongoose.connect('mongodb://localhost/botbox');

global.Player = require('./models/player')(app);

io.use(function(socket, next) {
    var req = socket.handshake;
    var res = {};
    cookieParser(req, res, function(err) {
        if (err) return next(err);
        session(req, res, next);
    });
});

require('./chat')(io);

var routes = require('./routes/home'), botbox = require('./routes/botbox');//, chat = require('./routes/chat');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method;
        delete req.body._method;
        return method;
    }
}));
app.use(express.Router());
app.use(cookieParser);
app.use(session);
app.use(express.static(path.join(__dirname, 'assets')));

app.use('/', routes);
app.use('/botbox', botbox);

app.use(function (req, res, next) {
    res.status(404);
    res.render('not-found');
});

if (app.get('env') === 'development') {

    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

io.set('authorization', function (data, accept) {
    cookieParser(data, {}, function (err) {
        var sessionID = data.signedCookies[KEY];
        store.get(sessionID, function (err, session) {
            if (err || !session) {
                accept(null, false);
            } else {
                data.session = session;
                accept(null, true);
            }
        });
    });
});

module.exports = app;

server.listen(3000, function () {
    console.log('Express server listening on port ' + server.address().port);
    debug('Express server listening on port ' + server.address());
});
