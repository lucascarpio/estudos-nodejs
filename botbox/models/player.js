module.exports = function (app) {
    var Schema = require('mongoose').Schema;

    var player = Schema({
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            index: {unique: true}
        },
        password: {
            type: String,
            required: true
        },
        robot: {
            name: String,
            color: String,
            velocity: Number,
            typeOfWeapon: String,
            defense: Number,
            health: Number
        },
        defeats: Number,
        victories: Number
    });

    return db.model('players', player);
}