var colorTextures = {};

function getTexture(color) {
    if (colorTextures[color] === undefined) {
        var canvas = document.createElement('canvas');
        canvas.width = 1;
        canvas.height = 1;
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#' + color.toString(16);
        ctx.beginPath();
        ctx.rect(0, 0, 1, 1);
        ctx.fill();
        ctx.closePath();
        colorTextures[color] = PIXI.Texture.fromCanvas(canvas);
    }
    return colorTextures[color];
}

function rectangle(x, y, width, height, backgroundColor, borderColor, borderWidth, velocity) {
    var box = new PIXI.DisplayObjectContainer();
    var border = new PIXI.Sprite(getTexture(borderColor));
    border.width = width + borderWidth;
    border.height = height + borderWidth;
    border.pivot.x = 0.5;
    border.pivot.y = 0.5;
    box.addChild(border);

    var background = new PIXI.Sprite(getTexture(backgroundColor));
    background.width = width - borderWidth;
    background.height = height - borderWidth;
    background.pivot.x = 0.5;
    background.pivot.y = 0.5;
    box.addChild(background);

    box.position.x = x;
    box.position.y = y;
    box.velocity = velocity || 1;

    return box;
}