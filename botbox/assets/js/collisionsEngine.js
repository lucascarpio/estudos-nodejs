function collisionsEngine(elements) {

    this.collisions = [];

    elements.forEach(function (entry) {
        this.collisions[elements.indexOf(entry)] = collisionsWith(entry);
    }, this);

    this.collisions.forEach(function (elementsCol, index) {
        elementsCol.forEach(function (entity) {
            if (elements[index].onCollision) elements[index].onCollision(elements[entity]);
            if (elements[entity].onCollision) elements[entity].onCollision(elements[index]);
        });
    }, this);

    function collisionsWith(entry) {
        var collisions = [];
        for (var i = elements.indexOf(entry) + 1; i < elements.length; i++) {
            if ((elements[i].collider && entry.collider) && elements[i].origin != elements.indexOf(entry) && elements[i].origin != entry.origin) {
                var distance = Parser.parse("sqrt(pow(((c1X+c1r)-(c2X+c2r)),2)+pow(((c1Y+c1r)-(c2Y+c2r)),2))-(c1r+c2r)").simplify({
                    c1X: entry.position.x - (entry.width / 2),
                    c1Y: entry.position.y - (entry.height / 2),
                    c1r: entry.width / 2,
                    c2X: elements[i].position.x - (elements[i].width / 2),
                    c2Y: elements[i].position.y - (elements[i].height / 2),
                    c2r: elements[i].width / 2
                });
                if (distance.evaluate() <= 0) {
                    collisions.push(i);
                }
            }
        }
        return collisions;
    }
}

