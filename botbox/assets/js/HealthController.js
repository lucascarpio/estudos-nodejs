function HealthController(robot, health) {
    this.health = health;
    this.damage = function (power) {
        if (this.health <= power) {
            this.health = 0;
            stage.removeChild(robot);
        } else {
            this.health -= power;
        }
        healthBar.width -= 50 * (power / 100);
    };
    var healthBar = rectangle(robot.position.x, robot.position.y - robot.height / 2 - 10, 50, 4, 0x10FF00, 0x000, 1);
    healthBar.layerCollision = 2;
    stage.addChild(healthBar);

    this.moveBar = function () {
        healthBar.position.x = robot.position.x;
        healthBar.position.y = robot.position.y - robot.height / 2 - 10;
    }

}