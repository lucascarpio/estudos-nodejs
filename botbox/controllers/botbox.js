module.exports = {
    index: function (req, res) {
        if (req.session.player) {
            var _id = req.session.player._id;
            Player.findById(_id, function (error, player) {
                var robot = player.robot;
                res.render('botbox/index', robot);
            });
        } else {
            res.redirect('/');
        }
    }
};
