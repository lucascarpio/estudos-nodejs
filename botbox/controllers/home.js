var crypto = require('crypto'),
    md5 = crypto.createHash('md5');
module.exports = {
    index: function (req, res) {
        res.render('home/index');
    },
    login: function (req, res) {
        var query = {email: req.body.email};
        Player.findOne(query).select('name email password').exec(function (erro, player) {
            md5 = crypto.createHash('md5');
            var password = md5.update(req.body.password).digest('hex');
            if (player && player.password == password) {
                req.session.player = player;
                res.redirect('/botbox');
            } else {
                res.redirect('/');
            }
        });
    },
    show_register: function (req, res) {
        res.render('home/register');
    },
    register: function (req, res) {
        md5 = crypto.createHash('md5');
        var password = md5.update(req.body.password).digest('hex');
        Player.create({name: req.body.name, email: req.body.email, password: password}, function (erro, player) {
            if (erro) {
                res.redirect('/');
            } else {
                req.session.player = player;
                res.redirect('/botbox');
            }
        });
    },
    logout: function (req, res) {
        req.session.destroy();
        res.redirect('/');
    }
};