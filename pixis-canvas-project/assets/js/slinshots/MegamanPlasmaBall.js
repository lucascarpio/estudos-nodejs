function MegaManPlasmaBall(robot, clickX, clickY, velocity) {
    var plasmaBall = rectangle(robot.position.x, robot.position.y, 10, 10, 0xFFFF19, 0xFF3300, 1, 1);
    plasmaBall.power = 5;
    plasmaBall.collider = true;
    plasmaBall.onCollision = function (el) {
        stage.removeChild(this);
    };


    plasmaBall.origin = stage.children.indexOf(robot);
    plasmaBall.position.x = robot.position.x;
    plasmaBall.position.y = robot.position.y;


    if (robot.position.x == clickX && robot.position.y == clickY) {
        stage.removeChild(plasmaBall);
        return;
    } else if (robot.position.x == clickX && robot.position.y > clickY) {
        plasmaBall.moveX = Parser.parse("0");
        plasmaBall.moveY = Parser.parse("-" + velocity);
    } else if (robot.position.x == clickX && robot.position.y < clickY) {
        plasmaBall.moveX = Parser.parse("0");
        plasmaBall.moveY = Parser.parse(velocity.toString());
    } else if (robot.position.x > clickX && robot.position.y == clickY) {
        plasmaBall.moveX = Parser.parse("-" + velocity);
        plasmaBall.moveY = Parser.parse("0");
    } else if (robot.position.x < clickX && robot.position.y == clickY) {
        plasmaBall.moveX = Parser.parse(velocity.toString());
        plasmaBall.moveY = Parser.parse("0");
    } else if (Math.abs(robot.position.x - clickX) <= Math.abs(robot.position.y - clickY)) {
        plasmaBall.moveX = Parser.parse(velocity + "*(abs(Xb-Xa)/abs(Yb-Ya))*((Xb-Xa)/abs(Xb-Xa))").simplify({
            Yb: clickY,
            Xb: clickX,
            Ya: robot.position.y,
            Xa: robot.position.x
        });

        plasmaBall.moveY = Parser.parse(velocity + "*((Yb-Ya)/ abs(Yb-Ya))").simplify({
            Yb: clickY,
            Xb: clickX,
            Ya: robot.position.y,
            Xa: robot.position.x
        });

    } else if (Math.abs(robot.position.x - clickX) > Math.abs(robot.position.y - clickY)) {
        plasmaBall.moveX = Parser.parse(velocity + "*((Xb-Xa)/ abs(Xb-Xa))").simplify({
            Xb: clickX,
            Xa: robot.position.x
        });

        plasmaBall.moveY = Parser.parse(velocity + "*(abs(Yb-Ya)/abs(Xb-Xa))*((Yb-Ya)/abs(Yb-Ya))").simplify({
            Yb: clickY,
            Xb: clickX,
            Ya: robot.position.y,
            Xa: robot.position.x
        });
    }

    plasmaBall.shooting = function () {
        plasmaBall.timer = setInterval(function () {
            shooting(plasmaBall);
        }, 1);
    }

    return plasmaBall;
}

function shooting(sprite) {
    if (sprite.position.x + 5 > renderer.width || sprite.position.x < 0 || sprite.position.y < 0 || sprite.position.y > renderer.height) {
        clearInterval(sprite.timer);
        stage.removeChild(sprite);
        return;
    }

    sprite.position.x = sprite.position.x + sprite.moveX.evaluate();
    sprite.position.y = sprite.position.y + sprite.moveY.evaluate();
    sprite.rotation += 0.2;

}