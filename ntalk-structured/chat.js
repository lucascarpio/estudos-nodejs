module.exports = function (io) {
    var crypto = require('crypto'),
        md5 = crypto.createHash('md5'),
        redis = require('redis').createClient(),
        sockets = io.sockets;

    sockets.on('connection', function (client) {
        var session = client.handshake.session,
            user = session.usuario;
        client.email = user.email;

        var onlines = sockets.sockets;
        onlines.forEach(function (online) {
            var email = online.email;
            if (email != null && email != user.email) {
                client.emit('notify-onlines', email);
                client.broadcast.emit('notify-onlines', email);
            }
        });

        client.on('join', function (sala) {
            if (sala) {
                sala = sala.replace('?', '')
            } else {

                var timestamp = new Date().toString(),
                    md5 = crypto.createHash('md5');
                sala = md5.update(timestamp).digest('hex');
            }
            client.sala = sala;
            client.join(sala);

            var msg = "<b>"+user.nome+":</b> entrou.<br>";
            redis.lpush(sala, msg, function(erro, res) {
                redis.lrange(sala, 0, -1, function(erro, msgs) {
                    msgs.forEach(function(msg) {
                        sockets.in(sala).emit('send-client', msg);
                    });
                });
            });
        });


        client.on('send-server', function (data) {
            var msg = "<b>" + user.nome + ":</b>" + data + "<br>";
            var room = client.sala;
            if (room) {
                redis.lpush(room, msg);
                var data = {email: user.email, sala: room};
                client.broadcast.emit('new-message', data);
                sockets.in(room).emit('send-client', msg);
            }
        });

        client.on('disconnect', function () {
            var room = client.sala;
            if (room) {
                var msg = "<b>" + user.nome + ":</b> saiu.<br>";
                redis.lpush(room, msg);
                client.broadcast.emit('notify-offline', user.email);
                client.in(room).emit('send-client', msg);
                client.leave(room);
            }
        });
    });
};
