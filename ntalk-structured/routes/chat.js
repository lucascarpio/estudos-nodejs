var router = require('express').Router();
var chatController = require('../controllers/chat');
var authenticate = require('../middleware/authenticator');
router.get('/:email', authenticate, chatController.index);
module.exports = router;