var router = require('express').Router();

var home = require('../controllers/home');

router.get('/', home.index);
router.post('/entrar', home.login);
router.get('/sair', home.logout);
module.exports = router;
