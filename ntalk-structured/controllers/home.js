module.exports = {
    index: function (req, res) {
        res.render('home/index');
    },
    login: function (req, res) {
        var query = {email: req.body.email};
        Usuario.findOne(query).select('nome email').exec(function (erro, usuario) {
            if (usuario) {
                req.session.usuario = usuario;
                res.redirect('/contatos');
            } else {
                Usuario.create({nome: req.body.nome, email: req.body.email}, function (erro, usuario) {
                    if (erro) {
                        res.redirect('/');
                    } else {
                        req.session.usuario = usuario;
                        res.redirect('/contatos');
                    }
                });
            }
        });
    },
    logout: function (req, res) {
        req.session.destroy();
        res.redirect('/');
    }
};